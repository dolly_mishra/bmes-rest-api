﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMESRestAPI.Models.Shared
{
	public enum ProductStatus
	{
		Inactive=0,
		Active=1
	}
}
