﻿using BMESRestAPI.Models.Product;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMESRestAPI.Database
{
	public class BmesDbContext:DbContext
	{
		public BmesDbContext(DbContextOptions<BmesDbContext> options) : base(options){}
		public DbSet<Brand> Brands { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Category> Categories { get; set; }

	}
}
